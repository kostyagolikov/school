<?php
$numbers = array(21, 86, 51, 68, 21, 31, 61, 83, 60, 70,
            29, 63, 65, 19, 12, 37, 95, 57, 54, 80,
            31, 18, 14, 66, 16, 21, 23, 8, 30, 20);
$sum = 0;
foreach ($numbers as $value) {
    if ($value % 2) {
        continue;
    }
    $sum += $value;
}
echo "Sum of even numbers:" .$sum;
?>