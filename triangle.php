<?php
$array = [
    [3,4,5],
    [4,2,2],
    [7,5,9],
    [2,10,7],
    [15,12,20],
    [2,4],
    [3,6,7,8],
    [4,8,12]
];
    function triangle($a, $b, $c) {
        if ($a + $b <= $c || 
            $a + $c <= $b || 
            $b + $c <= $a) {
                return false;
            } else {
            return true;
        }
    }
    foreach ($array as $row) {
        if (count($row) !==3) {
            echo "triangle must have have 3 sides; \n";
            continue;
        }
        list ($a, $b, $c) = $row;
        if (triangle($a, $b, $c)) {
            $s = ($a+$b+$c)/2;
            $st = sqrt($s*(($s-$a)*($s-$b)*($s-$c))); 
            echo "valid triangle with sides: $a, $b, $c and area: $st;";
        }
        else {
            echo "invalid triangle;";
        }
        echo "\n";
    }
?>